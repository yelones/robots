# robots

After cloning edit the .env file in the root folder and set up your mysql connection.
I have included an SQL dump in database/robots.sql to provide some test data.
Or you can create an empty database and run "php artisan migrate" in the root folder, if you have Laravel installed.
