<?php

namespace App\Http\Controllers\Api;

use App\Robot;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RobotController extends Controller
{
    public function list()
    {
        $robots = Robot::query()->withTrashed()->get();

        return response($robots, 200)
            ->header('Content-Type', 'Application/Json');
    }

    public function search($name)
    {
        $robots = Robot::where('name', 'LIKE', '%'.$name.'%');

        if ($robots->count()) {
            $code = 200;
            $response = [
                'status' => $code,
                'data' => $robots->get(),
            ];
        } else {
            $code = 404;
            $response = [
                'status' => $code,
                'message' => 'No robot could be found.',
            ];
        }

        return $this->response($response, $code);
    }

    public function filter($queryString)
    {
        // $robots = Robot::whereHas('type', function ($query) use ($queryString) {
        //     $query->where('name', 'LIKE', '%'.$queryString.'%');
        // })->where(function ($query) use ($queryString) {
        //     $query->where('status', 'LIKE', '%'.$queryString.'%');
        // });

        $robots = Robot::
            join('robot_types', 'robots.type_id', '=', 'robot_types.id')
            ->whereRaw('robots.name LIKE "%'.$queryString.'%" OR robot_types.name LIKE "%'.$queryString.'%"')
            ->selectRaw('robots.*, robot_types.name AS type');

        if ($robots->count()) {
            $code = 200;
            $response = [
                'status' => $code,
                'data' => $robots->get(),
            ];
        } else {
            $code = 404;
            $response = [
                'status' => $code,
                'message' => 'No robot could be found.',
            ];
        }

        return $this->response($response, $code);
    }

    public function find($id)
    {
        $robot = Robot::find($id);

        if ($robot) {
            $code = 200;
            $response = [
                'status' => $code,
                'data' => $robot,
            ];
        } else {
            $code = 404;
            $response = [
                'status' => $code,
                'message' => 'Robot could not be found.',
            ];
        }

        return $this->response($response, $code);
    }

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type_id' => 'required|numeric',
            'name' => 'required|unique:robots',
            'status' => 'required',
            'year' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $code = 409;
            $response = [
                'status' => $code,
                'message' => implode(' ', $validator->errors()->all()),
            ];
        } else {
            $code = 201;
            $response = [
                'status' => $code,
                'data' => Robot::create($request->all()),
            ];
        }

        return $this->response($response, $code);
    }

    public function put($id, Request $request)
    {
        $robot = Robot::find($id);

        if ($robot) {
            $validator = Validator::make($request->all(), [
                'type_id' => 'required|numeric',
                'name' => 'required|unique:robots,name,'.$robot->id,
                'status' => 'required',
                'year' => 'required|numeric'
            ]);
    
            if ($validator->fails()) {
                $code = 400;
                $response = [
                    'status' => $code,
                    'message' => implode(' ', $validator->errors()->all()),
                ];
            } else {
                $robot->update($request->all());

                $code = 201;
                $response = [
                    'status' => $code,
                    'data' => $robot,
                ];
            }
        } else {
            $code = 404;
            $response = [
                'status' => $code,
                'message' => 'Robot could not be found.',
            ];
        }

        return $this->response($response, $code);
    }

    public function delete($id)
    {
        $robot = Robot::find($id);

        if ($robot) {
            $robot->delete();

            $code = 200;
            $response = [
                'status' => $code,
                'data' => $robot,
            ];
        } else {
            $code = 404;
            $response = [
                'status' => $code,
                'data' => 'Robot could not be found.',
            ];
        }

        return $this->response($response, $code);
    }

    public function response($response, $code)
    {
        return response($response, $code)
            ->header('Content-Type', 'Application/Json');
    }
}
