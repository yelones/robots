<?php

namespace App\Http\Controllers\Api;

use App\RobotType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RobotTypeController extends Controller
{

    public function list()
    {
        $robotTypes = RobotType::all();

        return response($robotTypes, 200)
            ->header('Content-Type', 'application/json');
    }
}
