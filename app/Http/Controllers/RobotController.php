<?php

namespace App\Http\Controllers;

use App\Robot;
use Illuminate\Http\Request;

class RobotController extends Controller
{
    public function list()
    {
        $robots = Robot::whereNull('deleted_at')->orderBy('created_at', 'DESC');

        return view('overview')->with('robots', $robots);
    }
}
