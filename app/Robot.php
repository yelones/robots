<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Robot extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['type_id', 'name', 'status', 'year', 'deleted_at', 'created_at', 'updated_at'];

    public function type()
    {
        return $this->belongsTo('App\RobotType');
    }
}
