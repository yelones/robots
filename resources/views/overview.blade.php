<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <style>
            table {
                border-collapse: collapse;
            }

            table tr td, table tr th {
                border: 1px solid #aaa;
                padding: 3px;
            }
        </style>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th>Felvitel ideje</th>
                    <th>Név</th>
                    <th>Típus</th>
                    <th>Állapot</th>
                    <th>Gyártási év</th>
                </tr>
            </thead>
            @if ($robots->count())
                <tbody>
                    @foreach ($robots->get() as $robot)
                        <tr>
                            <td>{{ $robot->created_at }}</td>
                            <td>{{ $robot->name }}</td>
                            <td>{{ $robot->type->name }}</td>
                            <td>{{ $robot->status }}</td>
                            <td>{{ $robot->year }}</td>
                        </tr>
                    @endforeach
                </tbody>
            @endif
        </table>
    </body>
</html>
