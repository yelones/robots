<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/robots', 'Api\RobotController@list')->name('api.robots.list');
Route::get('robots/search/{name}', 'Api\RobotController@search')->name('api.robots.search');
Route::get('robots/filter', 'Api\RobotTypeController@list')->name('api.robot-types.list');
Route::get('robots/filter/{query}', 'Api\RobotController@filter')->name('api.robots.filter');
Route::get('robots/{id}', 'Api\RobotController@find')->name('api.robots.find');
Route::post('robots', 'Api\RobotController@post')->name('api.robots.post');
Route::put('robots/{id}', 'Api\RobotController@put')->name('api.robots.put');
Route::delete('robots/{id}', 'Api\RobotController@delete')->name('api.robots.delete');
